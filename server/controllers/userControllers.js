const connection = require('../../mysqldb')

exports.view = (req, res) => {
  
    let sql = "SELECT * FROM USER_TABLE";
    connection.query(sql,(err, rows)=>{
       // connection.close();
        if (err) throw err;
        res.render('home', {rows});
      // console.log(rows)
       // res.send(results);
    });
}

exports.find = (req, res) => {
    let searchTerm = req.body.search;
    let sql = 'SELECT * FROM USER_TABLE WHERE first_name LIKE ? OR last_name LIKE ?'
    connection.query(sql, ['%' + searchTerm + '%', '%' + searchTerm + '%'],(err, rows)=>{
       // connection.close();
        if (err) throw err;
        //console.log(rows)
        res.render('home', {rows});
      
    });

}

exports.form = (req, res) => {
    res.render('adduser');
}

exports.create = (req, res) => {
    const { first_name, last_name, email, phone, comments } = req.body;
    let sql = 'INSERT INTO USER_TABLE SET first_name = ?, last_name = ?, email = ?, phone = ?, comments = ?'
    connection.query(sql, [first_name, last_name, email, phone, comments],(err, rows)=>{
       // connection.close();
        if (err) throw err;
        //console.log(rows)
        res.render('adduser', { alert: 'User added successfully.' });
      
    });

}

exports.edit = (req, res) => {
    let sql = "SELECT * FROM USER_TABLE WHERE id = ?";
    connection.query(sql,[req.params.id],(err, rows)=>{
       // connection.close();
        if (err) throw err;
        res.render('edituser', {rows});
    });

}


exports.update = (req, res) => {
    const { first_name, last_name, email, phone, comments } = req.body;
    let sql = "UPDATE USER_TABLE SET first_name = ?, last_name = ?, email = ?, phone = ?, comments = ? WHERE id = ?";
    connection.query(sql, [first_name, last_name, email, phone, comments, req.params.id],(err, rows)=>{
       // connection.close();
       if (!err) {
        // User the connection
        connection.query('SELECT * FROM USER_TABLE WHERE id = ?', [req.params.id], (err, rows) => {
          // When done with the connection, release it
          
          if (!err) {
            res.render('edituser', { rows, alert: `${first_name} has been updated.` });
          } else {
            console.log(err);
          }
         console.log('The data from user table: \n', rows);
        });
      } else {
        console.log(err);
      }
    
        });
    

}


exports.delete = (req, res) => {
    let sql = "DELETE FROM USER_TABLE WHERE id = ?";
    connection.query(sql,[req.params.id],(err, rows)=>{
       // connection.close();
        if (err) throw err;
        res.redirect('/home');
    });

}

// View Users
exports.viewall = (req, res) => {

    // User the connection
    connection.query('SELECT * FROM USER_TABLE WHERE id = ?', [req.params.id], (err, rows) => {
      if (!err) {
        res.render('viewuser', { rows });
      } else {
      //  console.log(err);
      }
     // console.log('The data from user table: \n', rows);
    });
  
  }