const express = require('express');
const router = express.Router();
const usercontroller = require('../controllers/userControllers')

//create, find, update, delete

router.get('/home', usercontroller.view)
router.post('/home', usercontroller.find)
router.get('/adduser', usercontroller.form)
router.post('/adduser', usercontroller.create)

router.get('/edituser/:id', usercontroller.edit)
router.post('/edituser/:id', usercontroller.update)

router.get('/:id', usercontroller.delete)

router.get('/viewuser/:id', usercontroller.viewall)


router.get('/', (req, res) => {
    res.render('app');
});


module.exports = router;