const express = require('express')
const exphbs = require('express-handlebars');
const cors = require('cors');
const connection = require('./mysqldb')
require('dotenv').config()
const routes = require('./server/routes/userRoutes')
const app = express();
const port = process.env.PORT || 5000

//parsing middlewares // parse application/x-www-urlencoded
app.use(express.json());
app.use(express.urlencoded({ limit: "50mb", parameterLimit: 500000000, extended:true }));
app.use(cors({
    origin:'*',
      credentials:true
    }));

//static files
app.use(express.static('public'))

//templating engine

app.engine('handlebars', exphbs.engine());
app.set('view engine', 'handlebars');
app.set('views', './views');



//routes

app.use('/',routes)



//connection to server
app.listen(port,()=>{
    console.log(`server started at port ${port}`)
    connection.connect(function(err){
      if(err) throw err;
      console.log('Database connected!');
  })
});


